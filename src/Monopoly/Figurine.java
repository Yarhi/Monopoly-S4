package Monopoly;

import javax.swing.*;

/**
 * Created by gagz on 04/03/16.
 */
public class Figurine {
    public ImageIcon image;
    public String name;
    public int id;

    public Figurine (int id, String imagePath){
        this.id = id;
        ImageIcon tmp = new ImageIcon("assets/image/figurines/" + imagePath + ".png");
        this.image = new ImageIcon(tmp.getImage().getScaledInstance(50, 50,  java.awt.Image.SCALE_SMOOTH));
        this.image.setDescription(imagePath);
        this.name = imagePath;
    }
}
