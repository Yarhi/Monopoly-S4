package Monopoly;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {

    private View view;
    private MainView mainview;
    private NameView nameView;

    private Model model;

    private Map map;

    private Grid grid;

    public Dice dice;

    public Controller(View view, MainView mainView, NameView nView, Grid grid, Model model) {
        this.view = view;
        this.mainview = mainView;
        this.nameView = nView;
        this.grid = grid;

        this.model = model;
        this.dice = new Dice();

        nameView.setVisible(false);
        mainview.setVisible(false);

        //view listener
        view.getOk().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(view.getNbjoueurs().matches("[0-9]+")){
                    if(Integer.parseInt(view.getNbjoueurs()) <= 1)
                        JOptionPane.showMessageDialog(null, "Il doit au moins y avoir deux joueurs");
                    else{
                        model.nbJoueurs = Integer.parseInt(view.getNbjoueurs());
                        model.Create_player();
                        model.CreateFigurineList();
                        view.setVisible(false);
                        nameView.setVisible(true);
                    }
                }
                else{
                    JOptionPane.showMessageDialog(null, "Invalide");
                }
            }
        });

        nameView.getValidate().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(nameView.getPlayerName().getText().isEmpty() || nameView.getPlayerName().getText() == "")
                    JOptionPane.showMessageDialog(null, "Entrez un nom valide");
                else {
                    model.player.get(model.playerSetName).name = nameView.getPlayerName().getText();
                    nameView.setDescription("Entrez le nom du joueur " + (model.playerSetName + 2) + " : ");
                    nameView.getPlayerName().setText("");
                    model.playerSetName++;
                }
                if(model.playerSetName >= model.nbJoueurs){
                    nameView.setVisible(false);
                    mainView.setVisible(true);
                    map = new Map();
                    change_data();
                    mainView.getFinished_btn().setEnabled(false);
                    return;
                }
            }
        });

        //mainviewListner
        mainView.getPlay_btn().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                mainView.getFinished_btn().setEnabled(true);
                mainView.getPlay_btn().setEnabled(false);
                model.player().move();
                grid.change_val(model.player());
                verifyTransaction();
                /*if(getCurrent().getPlayer() != model.to_play && model.player().pos > 0 && model.player().pos < model.nbJoueurs){
                    System.out.println("Sur la case de " + model.player.get(model.player().pos).name + ", je lui dois " +
                            (100 * model.player.get(getCurrent().getPlayer())
                                    .getNbPropertyFromTheSameGroup(getCurrent().getGroup())));
                    model.player().lostMoney(
                            100 * model.player.get(getCurrent().getPlayer())
                                    .getNbPropertyFromTheSameGroup(getCurrent().getGroup())
                    );
                    model.player.get(getCurrent().getPlayer()).addMoney(
                            100 * model.player.get(getCurrent().getPlayer())
                                    .getNbPropertyFromTheSameGroup(getCurrent().getGroup())
                    );
                }*/
                change_data();
            }
        });

        mainView.getFinished_btn().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                mainView.getPlay_btn().setEnabled(true);
                model.to_play++;
                if(model.to_play > model.nbJoueurs - 1)
                    model.to_play = 0;
                change_data();
                mainView.getFinished_btn().setEnabled(false);
            }
        });

        mainview.getBuy_btn().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                getCurrent().setPlayer(model.to_play);
                model.player().buy(getCurrent());
                mainview.getBuy_btn().setEnabled(false);
                change_data();
            }
        });
    }

    private void verifyTransaction() {
            if (model.player().id != getCurrent().getPlayer() && getCurrent().getPlayer() != -1){
                int m = 100 * model.player.get(getCurrent().getPlayer()).getNbPropertyFromTheSameGroup(getCurrent().getGroup());
                model.player().lostMoney(m);
                model.player.get(getCurrent().getPlayer()).addMoney(m);
                System.out.println(model.player().name + " Donne " + m + " à " + model.player.get(getCurrent().getPlayer()).name);
            }

    }

    public void checkAvailable(){
        if(getCurrent().getPlayer() == -1
                && getCurrent().isAvailable()
                && (model.player().money - getCurrent().getValue() > 0))
            mainview.getBuy_btn().setEnabled(true);
        else
            mainview.getBuy_btn().setEnabled(false);
    }

    public void change_data() {
        checkAvailable();
        refreshTree();

        mainview.setPlayer_number(model.player().name);
        mainview.setCase_number(String.valueOf(model.player().pos));
        mainview.setCase_name(getCurrent().getName());
        mainview.setCase_value(String.valueOf(getCurrent().getValue()));
        mainview.setCase_available(String.valueOf(getCurrent().isAvailable()));
        if(getCurrent().getPlayer() != - 1)
            mainview.setCase_player(model.player.get(map.get_index(model.player().pos).getPlayer()).name);
        else
            mainview.setCase_player(" - ");
        mainview.setCase_group(String.valueOf(getCurrent().getGroup()));
        mainview.setDice_done(String.valueOf(model.player().last_play));
        mainview.setPlayer_money(String.valueOf(model.player().money) + " €");
        mainview.setImageFig(model.player().figurine);
    }

    private void refreshTree() {
        DefaultTreeModel model = (DefaultTreeModel) mainview.playerData.getModel();
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();

        root.removeAllChildren();
        model.reload(root);

        for (int i = 0; i < this.model.nbJoueurs; i++){
            DefaultMutableTreeNode playerTab = null;
            playerTab = new DefaultMutableTreeNode(this.model.player.get(i).name + " : " + this.model.player.get(i).money + " €");
            for (int j = 0; j < this.model.player.get(i).property.size(); j++){
                DefaultMutableTreeNode prop = null;
                prop = new DefaultMutableTreeNode(this.model.player.get(i).property.get(j).getName());
                prop.add(new DefaultMutableTreeNode("Group : " + this.model.player.get(i).property.get(j).getGroup()));
                prop.add(new DefaultMutableTreeNode("Value : " + this.model.player.get(i).property.get(j).getValue()));
                playerTab.add(prop);
            }
            root.add(playerTab);
        }
        model.reload(root);
        mainview.playerData.setModel(model);
    }

    public Property getCurrent(){
        return map.get_index(model.player().pos);
    }
}