package Monopoly;

import java.util.ArrayList;

public class Model {
    public int nbJoueurs;
    public int to_play = 0;
    public int playerSetName = 0;

    public FigurineList figurineList;
    public ArrayList<Player> player = new ArrayList<Player>();

    public Model(){

    }

    public void CreateFigurineList(){
        figurineList = new FigurineList();
    }

    public void Create_player(){
        for(int i = 0; i < nbJoueurs; i++) {
            player.add(new Player(i));
        }
    }

    public void setNbJoueurs(int nbJoueurs){
        this.nbJoueurs = nbJoueurs;
    }

    public Player player() {
        return player.get(to_play);
    }

    public boolean playerExist(int index){
        if(index < nbJoueurs && index > 0) return true;
        return false;
    }
}
