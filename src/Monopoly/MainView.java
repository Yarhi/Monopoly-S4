package Monopoly;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.awt.*;

public class MainView extends JFrame{
    private JPanel panel1;
    private JLabel Player_number;
    private JLabel Case_number;
    private JLabel Case_name;
    private JLabel Case_value;
    private JLabel Case_available;
    private JLabel Case_player;
    private JLabel Case_group;
    private JButton play_btn;
    private JButton buy_btn;
    private JButton finished_btn;
    private JLabel dice_done;
    private JLabel Player_money;
    private JLabel imageFig;
    private JPanel imageFigurine;
    public JTree playerData;

    private Model model;

    public MainView(){
        super("Monopoly");

        setContentPane(panel1);
        init();
        this.model = new Model();

        //Jtree options
        DefaultTreeModel model = (DefaultTreeModel)playerData.getModel();
        DefaultMutableTreeNode root = (DefaultMutableTreeNode)model.getRoot();
        root.removeAllChildren();
        playerData.setName("Player data");
        model.reload(root);

        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - 400) / 2);
        int y = (int) ((dimension.getHeight() - 200) / 2);
        ImageIcon ico = new ImageIcon("assets/image/icon.png");
        setIconImage(ico.getImage());
        setSize(600,300);
        setLocation(x, y);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public void init(){
        Player_number.setText("Player 1");
        Case_number.setText("x : 7 -- y : 0");
        Case_name.setText("Place de la bourse");
        Case_value.setText("260");
        Case_available.setText("1");
        Case_player.setText("NaN");
        Case_group.setText("6");
        dice_done.setText("3");

        playerData.clearSelection();
    }

    public JButton getPlay_btn(){
        return play_btn;
    }

    public JLabel getCase_number(){
        return Case_number;
    }

    public JLabel getCase_name(){
        return Case_name;
    }

    public JLabel getCase_value(){
        return Case_value;
    }

    public JLabel getCase_available(){
        return Case_available;
    }

    public JLabel getPlayer_number(){
        return Player_number;
    }

    public JLabel getCase_player(){
        return Case_player;
    }

    public JLabel getCase_group(){
        return Case_group;
    }

    public JLabel getDice_done(){
        return dice_done;
    }

    public JLabel getImageFig(){
        return imageFig;
    }


    public void setCase_number(String str){
        Case_number.setText(str);
    }

    public void setCase_name(String str){ Case_name.setText(str);
    }

    public void setCase_value(String str){
        Case_value.setText(str);
    }

    public void setCase_available(String str){
        Case_available.setText(str);
    }

    public void setPlayer_number(String str){
        Player_number.setText(str);
    }

    public void setCase_player(String str){
        Case_player.setText(str);
    }

    public void setCase_group(String str){
        Case_group.setText(str);
    }

    public void setDice_done(String str){
        dice_done.setText(str);
    }

    public void setPlayer_money(String str){
        Player_money.setText(str);
    }

    public JButton getBuy_btn() {
        return buy_btn;
    }

    public JButton getFinished_btn() {
        return finished_btn;
    }

    public void setImageFig(Figurine figurine){
        imageFig.setIcon(figurine.image);
    }

}
