package Monopoly;

import javax.swing.*;
import java.awt.*;

/**
 * Created by gagz on 04/03/16.
 */
public class NameView extends JFrame {
    private JLabel Description;
    private JTextField PlayerName;
    private JButton validate;
    private JPanel pane;

    public NameView(){
        super("Monopoly");

        setContentPane(pane);

        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - 400) / 2);
        int y = (int) ((dimension.getHeight() - 200) / 2);
        ImageIcon ico = new ImageIcon("assets/image/icon.png");
        setIconImage(ico.getImage());
        setSize(300,200);
        setLocation(x, y);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        setDescription("Entrez le nom du Joueur 1 :");
    }

    public JButton getValidate() {
        return validate;
    }

    public JLabel getDescription() {
        return Description;
    }

    public JTextField getPlayerName() {
        return PlayerName;
    }

    public void setDescription(String dsp){
        Description.setText(dsp);
    }
}
