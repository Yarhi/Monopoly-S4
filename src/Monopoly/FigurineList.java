package Monopoly;

import java.util.ArrayList;

/**
 * Created by gagz on 04/03/16.
 */
public class FigurineList {
    final static int DE = 0;
    final static int BROUETTE = 1;
    final static int SHOES = 2;
    final static int DOG = 3;
    final static int CAR = 4;
    final static int FER = 5;
    final static int HAT = 6;
    final static int BOAT = 7;

    public ArrayList<Figurine> list = new ArrayList<Figurine>();

    public FigurineList(){
        for (int i = 0; i <= 7; i++){
            if (i == 0)
                list.add(new Figurine(i, "de"));
            else if(i == 1)
                list.add(new Figurine(i, "brouette"));
            else if(i == 2)
                list.add(new Figurine(i, "shoes"));
            else if(i == 3)
                list.add(new Figurine(i, "dog"));
            else if(i == 4)
                list.add(new Figurine(i, "car"));
            else if(i == 5)
                list.add(new Figurine(i, "fer"));
            else if(i == 6)
                list.add(new Figurine(i, "hat"));
            else if(i == 7)
                list.add(new Figurine(i, "boat"));
        }
    }
}
