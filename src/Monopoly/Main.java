package Monopoly;

public class Main{
    public static void main(String[] args){
        Model model = new Model();
        Grid grid = new Grid(model);
        View view = new View();
        MainView mainView = new MainView();
        NameView nameView = new NameView();
        Controller controller = new Controller(view, mainView, nameView, grid,model);
    }
}