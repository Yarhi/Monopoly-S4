package Monopoly;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

/**
 * Created by gagz on 06/03/16.
 */
public class Grid extends JFrame{

    private Model model;

    public JPanel[] grid = new JPanel[40];
    public JPanel[] plat = new JPanel[40];

    public Map map;

    public Grid(Model model){
        this.model = model;
        //New instance of map
        map = new Map();

        int nbCase = -1;

        Toolkit tk = Toolkit.getDefaultToolkit();
        int xSize = ((int) tk.getScreenSize().getWidth());
        int ySize = ((int) tk.getScreenSize().getHeight());
        setSize(xSize,ySize);

        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - 400) / 2);
        int y = (int) ((dimension.getHeight() - 200) / 2);
        setLocation(x, y);

        ImageIcon ico = new ImageIcon("assets/image/icon.png");
        setIconImage(ico.getImage());

        setVisible(true);
        setTitle("Monopoly");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //CreatePlat
        JPanel pan = new JPanel (new GridLayout(11,11));

        Border blackline = BorderFactory.createLineBorder(Color.black,1);

        for (int i = 120; i >= 0 || nbCase != 39; i--){
            JPanel caseM = new JPanel();
            if(i > 110 || i < 10 || i%11 == 0 || (i-10)%11 == 0 ){
                nbCase++;
                Color group = get_color(map.get_index(nbCase).getGroup());

                caseM.add(getText(nbCase));
                caseM.setBackground(group);
                caseM.setBorder(blackline);
                grid[nbCase] = caseM;
            }
            pan.add(caseM);
        }

        add(pan);
        setVisible(true);
        setPlat();
    }

    private void setPlat(){
        plat[0] = grid[39];
        plat[1] = grid[38];
        plat[2] = grid[37];
        plat[3] = grid[36];
        plat[4] = grid[35];
        plat[5] = grid[34];
        plat[6] = grid[33];
        plat[7] = grid[32];
        plat[8] = grid[31];
        plat[9] = grid[30];
        plat[10] = grid[29];
        plat[11] = grid[27];
        plat[12] = grid[25];
        plat[13] = grid[23];
        plat[14] = grid[21];
        plat[15] = grid[19];
        plat[16] = grid[17];
        plat[17] = grid[15];
        plat[18] = grid[13];
        plat[19] = grid[11];
        plat[20] = grid[0];
        plat[21] = grid[1];
        plat[22] = grid[2];
        plat[23] = grid[3];
        plat[24] = grid[4];
        plat[25] = grid[5];
        plat[26] = grid[6];
        plat[27] = grid[7];
        plat[28] = grid[8];
        plat[29] = grid[9];
        plat[30] = grid[10];
        plat[31] = grid[12];
        plat[32] = grid[14];
        plat[33] = grid[16];
        plat[34] = grid[18];
        plat[35] = grid[20];
        plat[36] = grid[22];
        plat[37] = grid[24];
        plat[38] = grid[26];
        plat[39] = grid[28];
    }

    private Color get_color(int group) {
        if (group == 1)
            return new Color(156, 93, 82);
        else if (group == 2)
            return new Color(0, 255, 255);
        else if (group == 3)
            return new Color(153, 0, 153);
        else if (group == 4)
            return new Color(255, 128, 0);
        else if (group == 5)
            return new Color(255, 0, 0);
        else if (group == 6)
            return new Color(255, 255, 0);
        else if (group == 7)
            return new Color(0, 255, 0);
        else if (group == 8)
            return new Color(0, 0, 255);
        else if (group == 9)
            return new Color(255, 255, 255);
        else if (group == 10)
            return new Color(128, 128, 128);
        else if (group == 11)
            return new Color(255, 255, 255);

        return new Color(255, 255, 255);
    }

    public JTextArea getText(int index){
        System.out.println(index);
        JTextArea nameText = new JTextArea(
                map.get_index(index).getName() + "\n"
                        + map.get_index(index).getValue() + "€"
        );
        nameText.setEditable(false);

        Color group = get_color(map.get_index(index).getGroup());

        nameText.setLineWrap(true);
        nameText.setWrapStyleWord(true);
        nameText.setBackground(group);

        return nameText;
    }

    public void change_val(Player player){


        plat[player.last_pos].removeAll();
        plat[player.last_pos].add(getText(player.last_pos));
        plat[player.last_pos].repaint();

        plat[player.pos].removeAll();

        JLabel img = new JLabel(player.figurine.image);
        img.setOpaque(false);
        img.setComponentZOrder(plat[player.pos], 0);
        plat[player.pos].add(img);
        plat[player.pos].add(getText(player.pos));
        rootPane.revalidate();
    }
}
