package Monopoly;

import java.util.ArrayList;

/**
 * Created by gagz on 04/03/16.
 */
public class Player {
    public int pos;
    public int money;
    public int last_play = 0;
    public int last_pos;
    public int id;
    public Figurine figurine;
    public FigurineList figurineList;
    public String name;

    public ArrayList<Property> property = new ArrayList<Property>();

    public Player(int i){
        figurineList = new FigurineList();
        this.name = "";
        this.money = GameOptions.default_money;
        this.figurine = figurineList.list.get(new Dice().getRoll());
        this.id = i;
    }

    public void move(){
        int d = new Dice().getRoll();
        last_pos = pos;
        if (pos + d < 40)
            pos += d;
        else{
            money += 200;
            pos = pos + d - 40;
        }
        last_play = d;
    }

    public void buy(Property card) {
        money -= card.getValue();
        property.add(card);
    }

    public void addMoney(int value){money += value;}

    public void lostMoney(int value){money -= value;}

    public int getNbPropertyFromTheSameGroup(int group){
        int res = 0;
        for(int i = 0; i < property.size(); i++)
            if(property.get(i).getGroup() == group) res++;
        return res;
    }
}
