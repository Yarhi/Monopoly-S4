package Graphic;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

/**
 * Created by gagz on 06/03/16.
 */
public class Grid extends JFrame{

    public JPanel[] grid = new JPanel[40];

    public Grid(){
        setSize(400,200);                                // Fixe la taille par dfaut
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - 400) / 2);
        int y = (int) ((dimension.getHeight() - 200) / 2);
        ImageIcon ico = new ImageIcon("assets/image/icon.png");
        setIconImage(ico.getImage());
        setLocation(x, y);
        setVisible(true);                                // Affiche la fenetre
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// Gestion de la fermeture
        setTitle("Monopoly");

        JPanel pan = new JPanel (new GridLayout(10,10));
        Border blackline = BorderFactory.createLineBorder(Color.black,1);
        /*
        for(int i = 0; i<grid.length;i++){
            JPanel tmp = new JPanel();
            tmp.setBorder(blackline);
            tmp.setPreferredSize(new Dimension(50, 50));
            grid[i] = tmp;
            pan.add(grid[i]);
        }
        */
        add(pan);
        setVisible(true);

    }
}
